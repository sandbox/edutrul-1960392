
-- SUMMARY --

With Single User mode you can login once at a time.

Example: if you are logged in a machine and you want to log in again from another place, machine, browser, etc. You can not log in again because You are already logged in.

Core: Drupal 7
Project Page: http://drupal.org/sandbox/edutrul/1960392
Git: git clone 7.x-1.x edutrul@git.drupal.org:sandbox/edutrul/1960392.git single_user_mode

-- INSTALLATION --

1. Copy the Single User Mode module to your modules directory and enable it 
   on the Modules page (admin/modules).

2. Set Single User Mode permissions on the following page:
   (admin/people/permissions).


-- CONFIGURATION --


-- CREDITS --

Designed and Developed @edutrul
